express = require "express"
app = express()
port = process.env.PORT or 5000
{compile} = require "./helpers"
$ = require "jquery"

PUBLIC_DIR = __dirname + "/public"
APP_DIR = __dirname + "/app"
APP_3P_DIR = APP_DIR + "/3p"

# listen on port 5000
app.listen port, ->
    console.log "Listening on #{port}\n"

# set app configuration
app.configure ->
    # compile stylus to css on the fly
    app.use require("stylus").middleware
        # app/stylesheets
        src : APP_DIR
        # public/stylesheets
        dest : PUBLIC_DIR

    # serve public dir
    app.use express.static PUBLIC_DIR

# Compile coffeescript -> javascript
compile()

# landing page
app.get "/", (req, res) =>
    res.sendfile "app/views/index.html"

# stations
app.get "/stations", (req, res) =>
    $.get("http://citibikenyc.com/stations/json").always (data) -> res.send(data or {})
