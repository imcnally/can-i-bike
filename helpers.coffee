require "shelljs/global"
_ = require "underscore"

sh = (cmd) ->
    ###
        Execute shell command and exit properly
    ###
    if exec(cmd).code isnt 0
        console.log "Error: #{cmd} failed"
        exit 1

notice = (msg) ->
    ###
        Print a message highlighted with stars
    ###
    stars = ("*" for _ in [1..msg.length+4]).join("")
    console.log "#{stars}\n* #{msg} *\n#{stars}\n"

getFilesOfType = (fileExtension, directory) ->
    ###
        Return a list of all files with `fileExtension`
        contained in and below `directory`
    ###
    finder = require "findit"
    files = finder.sync(directory)
    pattern = new RegExp("\\.#{fileExtension}$", "g")
    file for file in files when file.match(pattern)?

compile = ->
    outputFile = "public/main.js"
    rm "-rf", outputFile
    files = _.unique(["app/app.coffee"].concat(getFilesOfType "coffee", "app")).join(" ")
    sh "coffee -l -j #{outputFile} -c #{files}"

module.exports = {sh, notice, compile}
