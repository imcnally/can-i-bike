app.controller "AppController", ($scope, Stations, Storage, analytics) ->

    $scope.stationsSearched = []
    $scope.lastUpdated = ""
    $scope.favorites = Storage.get("favorites") or []
    Storage.bind $scope, "favorites"
    $scope.search or= {}

    Stations.fetch()

    $scope.clearSearch = ->
        $scope.search.location = ""

    $scope.addFavorite = (station) ->
        $scope.favorites = _.unique [station.location].concat($scope.favorites)
        analytics.trackAddFavorite station.location

    $scope.removeFavorite = (stationName) ->
    	$scope.favorites = _.without $scope.favorites, stationName

    $scope.setStation = (stationName) ->
    	$scope.search.location = stationName

    $scope.$on "stationsSuccess", (_e, data) ->
        $scope.stationsSearched = data.stations
        $scope.lastUpdated = data.lastUpdated
