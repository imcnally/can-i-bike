ANALYTICS_ID = "UA-41506811-1"

createScriptElement = (url) ->
    element = document.createElement("script")
    element.type = "text/javascript"
    element.language = "javascript"
    element.src = url
    element

class AnalyticsService

    constructor : ->
        window._gaq = window._gaq or []
        isHttps = document.location.protocol is "https:"
        prefix = if isHttps then "https://ssl" else "http://www"
        element = createScriptElement("#{prefix}.google-analytics.com/ga.js")
        element.async = true
        firstScript = document.getElementsByTagName("script")[0]
        firstScript.parentNode.insertBefore(element, firstScript)
        window._gaq.push(["_setAccount", ANALYTICS_ID])

    trackPageView : ->
        window._gaq.push(["_trackPageview", "/"])

    trackEvent : (category, action, label) ->
        # category, action, label
        window._gaq.push(["_trackEvent", category, action, label])

    trackAddFavorite : (station) ->
        @trackEvent("Favorite", "Add", station)

app.factory "analytics", ->

    analyticsService = new AnalyticsService()
    analyticsService.trackPageView()
    trackAddFavorite = _.bind analyticsService.trackAddFavorite, analyticsService

    {trackAddFavorite}
