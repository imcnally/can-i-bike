parse = (response) ->
    lastUpdated = response.executionTime
    stations =
        for station in response.stationBeanList
            bikes : station.availableBikes
            docks : station.availableDocks
            location : station.stationName
            inService : !!station.statusKey
    {lastUpdated, stations}


app.factory "Stations", ($http, $rootScope) ->

    onError : =>
        console.log "error"

    onSuccess : (response) =>
        parsedResponse = parse response
        $rootScope.$broadcast "stationsSuccess", parsedResponse

    fetch : ->
        $http.get("/stations")
            .success(@onSuccess)
            .error(@onError)
